package codegenerator

import Option
import org.apache.bcel.Const.ACC_PUBLIC
import org.apache.bcel.classfile.ClassParser
import org.apache.bcel.classfile.JavaClass
import org.apache.bcel.generic.ClassGen
import org.apache.bcel.generic.InstructionList
import org.apache.bcel.generic.MethodGen
import org.apache.bcel.generic.Type
import java.io.*
import java.net.URL
import java.util.*
import javax.tools.JavaCompiler
import javax.tools.ToolProvider
import kotlin.collections.ArrayList

fun translateController(args: ArrayList<String>, options: ArrayList<Option>) {

    println("Start translating controller: ${args[0]}")

    if (args.size == 0)
        throw SemanticException("Missing command arguments", "uctrl")

    if (options.size != 0)
        throw SemanticException("Invalid command arguments", options.toString())

    val className = args[0]

    try {
        val isr = FileInputStream("${Configs.out}/main/java/${Configs.vendor}/controllers/$className.class")
        val c = loadClass(isr, className)

        val javaClass = c
        val modClass = ClassGen(javaClass)
        val methods = modClass.methods
        var construct: org.apache.bcel.classfile.Method? = null
        for (method in methods) {
            val annotations = method.annotationEntries
            for (annotation in annotations) {
                if (annotation.toString() == "@Lacolyte/RootConstruct;") {
                    construct = method
                }
            }
        }
        if (construct == null) {
            System.err.println("Controller doesn't have a root constructor")
            return
        }
        val list = MethodGen(construct, className, modClass.constantPool).instructionList
        val instructions = list.instructions
        val listNew = InstructionList()
        for (i in instructions.indices) {
            if (i != 0 && i != 1)
                listNew.append(instructions[i])
        }

        val init = MethodGen(
            ACC_PUBLIC.toInt(),
            Type.VOID,
            arrayOf(Type.getType(URL::class.java), Type.getType(ResourceBundle::class.java)),
            arrayOf("url", "resourceBundle"),
            "initialize",
            className,
            listNew,
            modClass.constantPool
        )

        modClass.addInterface("javafx.fxml.Initializable")
        modClass.removeMethod(methods[0])
        modClass.addEmptyConstructor(ACC_PUBLIC.toInt())

        init.setMaxLocals()
        init.setMaxStack()
        modClass.addMethod(init.method)
        modClass.update()

        val newClass = modClass.javaClass
        newClass.dump("${Configs.out}/main/java/${Configs.vendor}/controllers/$className.class")
    } catch (exc: Exception) {
        exc.printStackTrace()
    }
}

//fun checkController(control : JavaClass) : Boolean{
//
//}

fun loadClass(`is`: InputStream?, className: String): JavaClass {
    try {
        if (`is` != null) {
            val parser = ClassParser(`is`, className)
            val clazz = parser.parse()
            return clazz
        }
    } catch (var15: IOException) {
        throw ClassNotFoundException("Exception while looking for class $className: $var15", var15)
    } finally {
        if (`is` != null) {
            try {
                `is`.close()
            } catch (var14: IOException) {
            }
        }
    }
    throw ClassNotFoundException("SyntheticRepository could not load $className")
}