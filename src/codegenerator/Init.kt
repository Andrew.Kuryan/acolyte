package codegenerator

import java.nio.file.Files
import java.nio.file.Paths
import Option
import java.nio.charset.Charset

fun init(args : ArrayList<String>, options : ArrayList<Option>){
    if (args.size != 0)
        throw SemanticException("Invalid command arguments", args.toString())

    val name = Paths.get(debug).toAbsolutePath().fileName.toString()
    val out = "out/production/$name"
    var vendor = "org/mt"
    var title = name

    var mode = 1

    for (option in options){
        when (option.o){
            in setOf("v", "vendor") ->{
                if (option.args.size == 0)
                    throw SemanticException("Option arguments were expected", option.o)
                val paths = option.args[0].split(".")
                vendor = ""
                for (p in paths.reversed())
                    vendor += "$p/"
                vendor = vendor.removeSuffix("/")
            }
            in setOf("kt", "kotlin") -> mode = 2
            in setOf("t", "title") -> title = option.args[0]
            else -> throw SemanticException("Invalid command arguments", option.o)
        }
    }

    Files.createDirectories(Paths.get("${debug}src/main"))
    Files.createDirectories(Paths.get("${debug}src/acolyte"))

    Files.createDirectories(Paths.get("${debug}src/main/resources"))
    Files.createDirectories(Paths.get("${debug}src/main/resources/layout"))
    Files.createDirectories(Paths.get("${debug}src/main/resources/drawable"))
    Files.createDirectories(Paths.get("${debug}src/main/resources/styles"))

    Files.createDirectories(Paths.get("${debug}src/main/java/$vendor"))
    Files.createDirectories(Paths.get("${debug}src/main/java/$vendor/controllers"))
    Files.createDirectories(Paths.get("${debug}src/main/java/$vendor/interfaces"))
    Files.createDirectories(Paths.get("${debug}src/main/java/$vendor/models"))

    val config = arrayListOf(
"""{
  "project_name":"$name",
  "output_path":"$out",

  "vendor":"$vendor",
  "title":"$title"
}"""
    )

    val jControlAnnot = arrayListOf(
"""package acolyte;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Controller {
}"""
    )

    val jConstructAnnot = arrayListOf(
        """package acolyte;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = ElementType.CONSTRUCTOR)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface RootConstruct {
}"""
    )

    val jHandlerAnnot = arrayListOf(
        """package acolyte;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = ElementType.METHOD)
@Retention(value = RetentionPolicy.SOURCE)
public @interface Handler {
    String target();
    String type();
}"""
    )

    val jModel = arrayListOf(
"""package acolyte;

import javafx.fxml.FXMLLoader;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class Model extends FXMLLoader {

    private String name;

    public Model(String name){
        this.name = name;
        translateController(name.concat("Controller"));
        setLocation(getClass().getResource("../../../../resources/layout/".concat(name.toLowerCase()).concat("view.fxml")));
        try {
            load();
        }catch (IOException exc){
            exc.printStackTrace();
        }
    }

    @Override
    public <T> T getRoot(){
        if (getController() == null){
            return null;
        }
        else{
            return super.getRoot();
        }
    }

    @Override
    public <T> T getController() {
        return super.getController();
    }

    private static void translateController(String className){
        String[] compile = new String[] {"java", "-jar", "acolyte.jar", "tctrl", className};
        try {
            Process process = new ProcessBuilder(compile).start();
            while (process.isAlive()){
                //ожидание завершения процесса
            }
        }catch (Exception exc){
            exc.printStackTrace();
        }
    }
}"""
    )

    val jMain = arrayListOf(
"""package main.java.${vendor.replace("/", ".")};

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage){
        primaryStage.setTitle("$title");
        primaryStage.show();
    }

    public static void main(String args[]){
        launch(args);
    }
}"""
    )

    val kMain = arrayListOf(
"""package main.java.${vendor.replace("/", ".")}

import javafx.application.Application
import javafx.stage.Stage

class Main : Application() {

    override fun start(primaryStage: Stage?) {
        primaryStage?.title = "$title"
        primaryStage?.show()
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(Main::class.java)
        }
    }
}"""
    )

    //файл описания
    val file = Paths.get("${debug}mtmvc-config.json")
    Files.write(file, config, Charset.forName("UTF-8"))

    //вспомогательные файлы
    Files.write(Paths.get("${debug}src/acolyte/Model.java"),
        jModel, Charset.forName("UTF-8"))
    Files.write(Paths.get("${debug}src/acolyte/Controller.java"),
        jControlAnnot, Charset.forName("UTF-8"))
    Files.write(Paths.get("${debug}src/acolyte/RootConstruct.java"),
        jConstructAnnot, Charset.forName("UTF-8"))
    Files.write(Paths.get("${debug}src/acolyte/Handler.java"),
        jHandlerAnnot, Charset.forName("UTF-8"))

    when (mode){
        1 -> {
            val fileC = Paths.get("${debug}src/main/java/$vendor/Main.java")
            Files.write(fileC, jMain, Charset.forName("UTF-8"))
        }
        2 -> {
            val fileC = Paths.get("${debug}src/main/java/$vendor/Main.kt")
            Files.write(fileC, kMain, Charset.forName("UTF-8"))
        }
    }

    println("Init project structure")
}