package codegenerator

import Option
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

fun updateController(args : ArrayList<String>, options : ArrayList<Option>){

    if (args.size == 0)
        throw SemanticException("Missing command arguments", "uctrl")

    if (options.size != 0)
        throw SemanticException("Invalid command arguments", options.toString())

    val fullName = args[0]
    val viewName = "${args[0].substring(0, 1).toLowerCase()}${args[0].substring(1).removeSuffix("Controller")}view"

    //считывание всех строк их fxml-файла
    val fxml = Paths.get("${debug}src/main/resources/layout/$viewName.fxml")
    val linesFxml = Files.readAllLines(fxml, Charset.forName("UTF-8"))

    //парсинг fxml-файла
    var curLevel = 0
    val components = linkedMapOf<String, Pair<String, Int>>()
    val handlers = linkedMapOf<String, Pair<String, String>>()
    for (line in linesFxml) {
        val posCompStart = line.indexOf("<")
        var posCompEnd = posCompStart + 1
        if (posCompStart != -1) {
            while (line[posCompEnd] != ' ' && line[posCompEnd] != '>') {
                posCompEnd++
            }
        }
        var tag = ""
        if (posCompStart != -1 && posCompEnd != 0) {
            tag = line.substring(posCompStart + 1, posCompEnd)
            println(tag)
        }
        if (tag == "children")
            curLevel++
        else if (tag == "/children")
            curLevel--
        val posId = line.lastIndexOf("fx:id")
        if (posId != -1) {
            val posIdStart = line.indexOf("\"", posId)
            val posIdEnd = line.indexOf("\"", posIdStart + 1)
            components[line.substring(posIdStart + 1, posIdEnd)] = tag to curLevel
        }
        var posHandler = line.indexOf("#")
        while (posHandler != -1){
            val posHandlerEnd = line.indexOf("\"", posHandler)
            val posTypeEnd = posHandler-2
            var posTypeStart = posTypeEnd
            while (line[posTypeStart] != ' '){
                posTypeStart--
            }
            handlers[line.substring(posHandler+1, posHandlerEnd)] = line.substring(posTypeStart+1, posTypeEnd) to tag
            posHandler = line.indexOf("#", posHandler+1)
        }
    }
    if (debug.isNotEmpty()) {
        println("Components: $components")
        println("Handlers: $handlers")
    }

    //считывание всех строк из файла контроллера
    val control = Paths.get("${debug}src/main/java/${Configs.vendor}/controllers/$fullName.java")
    val linesCtrl = Files.readAllLines(control, Charset.forName("UTF-8"))

    //разбиение строк контроллера на идентификаторы
    val map = ArrayList<Pair<String, Int>>()
    val reg = Pattern.compile("@?\\b[A-Za-z_]\\w*")
    for ((index, line) in linesCtrl.withIndex()) {
        val match = reg.matcher(line)
        var pos = 0
        while (match.find(pos)) {
            val str = line.substring(match.start(), match.end())
            map += (str to index)
            pos = match.end()
        }
    }
    //парсинг контроллера
    var flagFxml = false
    var flagHandle = false
    var initPos = -1
    var listenPos = -1
    val linesToRemove = sortedSetOf<Int>()
    val unusedHandlers = sortedMapOf<String, String>()
    val addedComponents = sortedMapOf<String, String>()
    for ((component, description) in components){
        addedComponents += component to description.first
    }
    val removedComponents = sortedMapOf<String, String>()
    for ((index, pair) in map.withIndex()){
        //println(pair.first)
        when (pair.first){
            "@RootConstruct" -> initPos = pair.second
            "setListener" -> listenPos = pair.second
            "@FXML" -> flagFxml = true
            "@Handler" -> flagHandle = true
            else -> {
                if (flagFxml){
                    if (map[index+1].first in components){
                        addedComponents -= map[index+1].first
                        //components -= map[index+1].first
                        linesToRemove += map[index-1].second
                        linesToRemove += map[index].second
                    }
                    else{
                        removedComponents[map[index+1].first] = map[index].first
                        linesToRemove += map[index-1].second
                        linesToRemove += map[index].second
                    }
                    flagFxml = false
                }
                if (flagHandle){
                    val target = map[index+1].first
                    val type = map[index+3].first
                    val name = map[index+6].first
                    if (handlers[name]?.first == type && handlers[name]?.second == target){
                        handlers -= name
                    }
                    else{
                        unusedHandlers += name to type
                    }
                    flagHandle = false
                }
            }
        }
    }

    //вывод информации об изменениях
    if (components.size != 0) {
        println("Added components:")
        for ((id, comp) in addedComponents) {
            println("\t$comp $id")
        }
    }
    if (removedComponents.size != 0){
        println("Removed components:")
        for ((id, comp) in removedComponents) {
            println("\t$comp $id")
        }
    }
    if (handlers.size != 0) {
        println("Added handlers:")
        for (handler in handlers) {
            println("\t${handler.value.first} ${handler.key}()")
        }
    }
    if (unusedHandlers.size != 0){
        println("Unused handlers:")
        for (handler in unusedHandlers){
            println("\t${handler.value} ${handler.key}()")
        }
    }

    //запись изменений в файл
    for ((index, line) in linesToRemove.withIndex()){
        linesCtrl.removeAt(line-index)
        initPos--
        listenPos--
    }
    var curPos = initPos-1
    while (linesCtrl[curPos].trim().isEmpty()){
        curPos--
    }
    curPos++
    if (linesCtrl[curPos].isNotEmpty()){
        linesCtrl.add(curPos, "")
    }
    curPos++
    for ((id, comp) in components){
        fun printTabs(amount : Int) : String{
            var res = ""
            for (i in 0 until amount){
                res += "\t"
            }
            return res
        }
        linesCtrl.addAll(curPos, arrayListOf(
            "\t${printTabs(comp.second)}@FXML",
            "\t${printTabs(comp.second)}${comp.first} $id;"
        ))
        curPos += 2
        listenPos += 2
    }
    curPos = listenPos
    for ((method, description) in handlers){
        linesCtrl.addAll(curPos, arrayListOf(
            "\t@Handler(target = \"${description.second}\", type = \"${description.first}\")",
            "\tpublic void $method() {",
            "",
            "\t}"
        ))
        curPos += 4
    }
    if (handlers.size != 0){
        linesCtrl.add(curPos, "")
    }

    Files.write(control, linesCtrl, Charset.forName("UTF-8"))
}