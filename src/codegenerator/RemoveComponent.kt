package codegenerator

import Option
import java.nio.file.Files
import java.nio.file.Paths

fun removeComponent(args : ArrayList<String>, options : ArrayList<Option>){

    if (args.size == 0)
        throw SemanticException("Missing command arguments", "rc")

    var mode = 1
    for (option in options){
        when (option.o){
            in setOf("kt", "kotlin") -> mode = 2
            else -> throw SemanticException("Invalid command arguments", option.o)
        }
    }

    var name = args[0].substring(0, 1).toUpperCase()
    name += args[0].substring(1)

    val fileV = Paths.get("${debug}src/main/resources/layout/${args[0]}view.fxml")
    Files.delete(fileV)

    when (mode){
        1 -> {
            val fileC = Paths.get("${debug}src/main/java/${Configs.vendor}/controllers/${name}Controller.java")
            Files.delete(fileC)
            val fileM = Paths.get("${debug}src/main/java/${Configs.vendor}/models/${name}Model.java")
            Files.delete(fileM)
            val fileI = Paths.get("${debug}src/main/java/${Configs.vendor}/interfaces/${name}Listener.java")
            Files.delete(fileI)
        }
        2 -> {
            val fileC = Paths.get("${debug}src/main/java/${Configs.vendor}/controllers/${name}Controller.kt")
            Files.delete(fileC)
            val fileM = Paths.get("${debug}src/main/java/${Configs.vendor}/models/${name}Model.kt")
            Files.delete(fileM)
            val fileI = Paths.get("${debug}src/main/java/${Configs.vendor}/interfaces/${name}Listener.kt")
            Files.delete(fileI)
        }
    }
    println("Remove component: $name")
}