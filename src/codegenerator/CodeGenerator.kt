package codegenerator

import Command
import Option

const val debug = "testproject/"

class SemanticException(val s : String, val tok : String) : Exception(s){

    override fun toString() = "Semantic error: $s: $tok"
}

val functions : Map<String, (ArrayList<String>, ArrayList<Option>)->Unit> = mapOf(
    "init" to ::init,
    "nc" to ::newComponent,
    "rc" to ::removeComponent,
    "uctrl" to ::updateController,
    "tctrl" to ::translateController
)

fun generate(command: Command){
    try {
        functions[command.c]?.invoke(command.args, command.options)
    }catch (exc : Exception){
        throw exc
    }
}