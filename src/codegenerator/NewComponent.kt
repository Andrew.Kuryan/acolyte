package codegenerator

import Configs
import Option
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Paths

fun newComponent(args : ArrayList<String>, options : ArrayList<Option>){

    if (args.size == 0)
        throw SemanticException("Missing command arguments", "nc")

    var mode = 1
    for (option in options){
        when (option.o){
            in setOf("kt", "kotlin") -> mode = 2
            else -> throw SemanticException("Invalid command arguments", option.o)
        }
    }
    var name = args[0].substring(0, 1).toUpperCase()
    name += args[0].substring(1)


    val fxml = arrayListOf(
"""<?xml version="1.0" encoding="UTF-8"?>

<?import javafx.scene.layout.AnchorPane?>

<AnchorPane fx:id="root" maxHeight="-Infinity" maxWidth="-Infinity" minHeight="-Infinity" minWidth="-Infinity"
        prefHeight="400.0" prefWidth="600.0"
        xmlns="http://javafx.com/javafx/10.0.1" xmlns:fx="http://javafx.com/fxml/1"
        fx:controller="main.java.${Configs.vendor.replace("/", ".")}.controllers.${name}Controller"/>"""
    )

    val jControl = arrayListOf(
"""package main.java.${Configs.vendor.replace("/", ".")}.controllers;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

import acolyte.Controller;
import acolyte.RootConstruct;
import acolyte.Handler;
import main.java.${Configs.vendor.replace("/", ".")}.interfaces.${name}Listener;

@Controller
public class ${name}Controller{

    private ${name}Listener listener;

    @FXML
    AnchorPane root;

    @RootConstruct
    ${name}Controller(){

    }

    public void setListener(${name}Listener listener){
        this.listener = listener;
    }
}"""
    )

    val kControl = arrayListOf(
"""package main.java.${Configs.vendor.replace("/", ".")}.controllers

import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.layout.AnchorPane

import java.net.URL
import java.util.ResourceBundle

import acolyte.Controller
import main.java.${Configs.vendor.replace("/", ".")}.interfaces.${name}Listener

@Controller
class ${name}Controller : Initializable {

    lateinit var listener : ${name}Listener

    @FXML
    lateinit var root : AnchorPane

    override
    fun initialize(url : URL, resourceBundle : ResourceBundle?){

    }
}"""
    )

    val jModel = arrayListOf(
"""package main.java.${Configs.vendor.replace("/", ".")}.models;

import acolyte.Model;
import main.java.${Configs.vendor.replace("/", ".")}.controllers.${name}Controller;
import main.java.${Configs.vendor.replace("/", ".")}.interfaces.${name}Listener;

public class ${name}Model extends Model implements ${name}Listener {

    private ${name}Controller controller;

    public ${name}Model() {
        super("$name");
        controller = getController();
        controller.setListener(this);
    }
}"""
    )

    val kModel = arrayListOf(
"""package main.java.${Configs.vendor.replace("/", ".")}.models

import acolyte.Model
import main.java.${Configs.vendor.replace("/", ".")}.controllers.${name}Controller
import main.java.${Configs.vendor.replace("/", ".")}.interfaces.${name}Listener

class ${name}Model
    : Model("$name"), ${name}Listener {

    private val controller: ${name}Controller = getController<${name}Controller>()

    init {
        controller.listener = this
    }
}"""
    )

    val jInterf = arrayListOf(
"""package main.java.${Configs.vendor.replace("/", ".")}.interfaces;

public interface ${name}Listener {

}"""
    )

    val kInterf = arrayListOf(
"""package main.java.${Configs.vendor.replace("/", ".")}.interfaces

interface ${name}Listener {

}"""
    )

    val fileV = Paths.get("${debug}src/main/resources/layout/${args[0]}view.fxml")
    Files.write(fileV, fxml, Charset.forName("UTF-8"))

    when (mode){
        1 -> {
            val fileC = Paths.get("${debug}src/main/java/${Configs.vendor}/controllers/${name}Controller.java")
            Files.write(fileC, jControl, Charset.forName("UTF-8"))
            val fileM = Paths.get("${debug}src/main/java/${Configs.vendor}/models/${name}Model.java")
            Files.write(fileM, jModel, Charset.forName("UTF-8"))
            val fileI = Paths.get("${debug}src/main/java/${Configs.vendor}/interfaces/${name}Listener.java")
            Files.write(fileI, jInterf, Charset.forName("UTF-8"))
        }
        2 -> {
            val fileC = Paths.get("${debug}src/main/java/${Configs.vendor}/controllers/${name}Controller.kt")
            Files.write(fileC, kControl, Charset.forName("UTF-8"))
            val fileM = Paths.get("${debug}src/main/java/${Configs.vendor}/models/${name}Model.kt")
            Files.write(fileM, kModel, Charset.forName("UTF-8"))
            val fileI = Paths.get("${debug}src/main/java/${Configs.vendor}/interfaces/${name}Listener.kt")
            Files.write(fileI, kInterf, Charset.forName("UTF-8"))
        }
    }
    println("Create new component: $name")
}