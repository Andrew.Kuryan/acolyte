import codegenerator.debug
import java.io.FileNotFoundException
import java.io.FileReader
import javax.json.Json

object Configs{
    var vendor : String = ""
    var name : String = ""
    var out : String = ""

    fun readConfigs(){
        try {
            val iStream = FileReader("${debug}mtmvc-config.json")
            val reader = Json.createReader(iStream)
            val obj = reader.readObject()
            name = obj.getString("project_name")
            out = obj.getString("output_path")
            vendor = obj.getString("vendor")
        }catch (exc : FileNotFoundException){}
    }
}

