import codegenerator.debug
import java.lang.Exception
import java.lang.StringBuilder

open class Production(val s : String,
                      val args: ArrayList<String>)

class Command(val c : String,
              var options : ArrayList<Option> = ArrayList()) : Production(c, ArrayList()){

    override fun toString(): String {
        val s = StringBuilder("Command ")
        s.append(c).append(" args: ").append(args)
        for (option in options)
            s.append("\n").append(option.toString())
        return s.toString()
    }
}

class Option(val o : String) : Production(o, ArrayList()){

    override fun toString(): String = "Option $o args: $args"
}

class SyntaxException(val s : String, val tok : String) : Exception(s){

    override fun toString() = "Syntax error: $s: $tok"
}

fun parse(tokens : ArrayList<Token>) : Command?{
    var curCommand : Command? = null
    var curOption : Option
    var cur : Production? = null
    for (token in tokens){
        when(token.t){
            Type.COMMAND -> {
                if (curCommand == null)
                    curCommand = Command(token.s)
                else
                    throw SyntaxException("Invalid context of using command", token.s)
                cur = curCommand
            }
            Type.OPTION -> {
                curOption = Option(token.s)
                if (curCommand != null)
                    curCommand.options.add(curOption)
                else
                    throw SyntaxException("Using options without a command", token.s)
                cur = curOption
            }
            Type.ARG -> {
                if (cur != null)
                    cur.args.add(token.s)
                else
                    throw SyntaxException("Unknown identifier", token.s)
            }
        }
    }
    if (debug.isNotEmpty())
        println(curCommand)
    return curCommand
}