import codegenerator.debug

val commands = arrayListOf("init", "nc", "uctrl", "rc", "tctrl")

val shortOptions = arrayListOf("-kt", "-v", "-t")
val fullOptions = arrayListOf("--kotlin", "--vendor", "--title")

enum class Type{
    COMMAND, OPTION, ARG
}

data class Token(val s : String, val t : Type){
    override fun toString() = "[$s, $t]"
}

class LexicException (val s : String, val tok : String) : Exception(s){

    override fun toString() = "Lexical error: $s: $tok"
}

fun analyze(line : Array<String>?) : ArrayList<Token>{
    if (line == null)
        throw LexicException("Missing any commands", "acolyte")
    val out = ArrayList<Token>()
    for (s in line){
        when (s){
            in commands -> out.add(Token(s, Type.COMMAND))
            in shortOptions -> out.add(Token(s.removePrefix("-"), Type.OPTION))
            in fullOptions -> out.add(Token(s.removePrefix("--"), Type.OPTION))
            else -> {
                if (s.startsWith("-") || s.startsWith("--"))
                    throw LexicException("Unknown option", s)
                out.add(Token(s, Type.ARG))
            }
        }
    }
    if (debug.isNotEmpty())
        println(out)
    return out
}