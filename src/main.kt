import codegenerator.generate

fun main(args : Array<String>){
    Configs.readConfigs()
    val tokens = try {
        analyze(args)
    }catch (exc : LexicException){
        println(exc)
        return
    }

    val command = try {
        parse(tokens)
    }catch (exc : SyntaxException){
        println(exc)
        return
    }

    try{
        generate(command!!)
    }catch (exc : Exception){
        println(exc)
    }
}